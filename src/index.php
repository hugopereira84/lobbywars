<?php

require __DIR__.'/vendor/autoload.php';

use Entity\Contract;
use Exception\InvalidSigns;

try{
    $contract = new Contract('KNV###', 'NNV');
    echo '<br>Winner: '.$contract->getWinner().'. Dispute between: KNV### and NNV';
}catch (InvalidSigns $exception){
    echo '<br>Error: '.$exception->getMessage();
}



try{
    $contract = new Contract('KNV#', 'NNKKV#');
    echo '<br>Winner: '.$contract->getWinner().'. Dispute between: KNV# and NNKKV#';
}catch (InvalidSigns $exception){
    echo '<br>Error: '.$exception->getMessage().'. Dispute between: KNV# and NNKKV#';
}
