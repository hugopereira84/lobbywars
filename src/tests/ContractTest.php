<?php

use Entity\Contract;
use Exception\InvalidSigns;
use PHPUnit\Framework\TestCase as TestCaseAlias;


class ContractTest extends TestCaseAlias
{
    /**
     * @dataProvider additionProvider
     * @param string $plaintiff
     * @param string $defendant
     * @param string $expected
     */
    public function testGetWinner(string $plaintiff, string $defendant, string $expected)
    {
        $contract = new Contract($plaintiff, $defendant);
        $this->assertSame($expected, $contract->getWinner());
    }

    public function testValidateEmptySignatureFirstPart()
    {
        $this->expectException(InvalidSigns::class);
        (new Contract('KNV##', "KNV#"))->getWinner();
    }

    public function testValidateEmptySignatureSecondPart()
    {
        $this->expectException(InvalidSigns::class);
        (new Contract('KNV#', "KNV##"))->getWinner();
    }

    public function additionProvider()
    {
        return [
            ['KN', 'NNV', 'KN'],
            ['KV', 'KNN', 'KNN'],
            ['NV', 'V', 'NV'],
            ['K', 'V', 'K'],

            ['KNV#', 'KNVKN#', 'KNVKN#'],
            ['KN#', 'K#', 'KN#']
        ];
    }
}
