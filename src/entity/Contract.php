<?php

namespace Entity;

use Entity\Roles\BaseRole;
use Entity\Roles\King;
use Entity\Roles\Notary;
use Entity\Roles\Validator;
use Exception\InvalidSigns;

/**
 * Class Contract
 */
class Contract
{
    /**
     * @var string
     */
    protected $plaintiff;

    /**
     * @var string
     */
    protected $defendant;

    /**
     * Contract constructor.
     * @param string $plaintiff
     * @param string $defendant
     */
    public function __construct(string $plaintiff, string $defendant)
    {
        $this->plaintiff = $plaintiff;
        $this->defendant = $defendant;
    }

    /**
     * @return string
     */
    public function getWinner():string {
        if(false === $this->areValidateSigns()){
            throw new InvalidSigns('You have more then one #. Dispute is between:'.$this->plaintiff.' and '.$this->defendant);
        }
        $plaintiff = $this->getPoints($this->plaintiff);
        $defendant = $this->getPoints($this->defendant);

        return $plaintiff > $defendant ? $this->plaintiff : $this->defendant;
    }

    /**
     * @return bool
     */
    private function areValidateSigns():bool {
        $isValidPlaintiff = array_count_values(str_split($this->plaintiff))['#'] <= 1;
        $isValidDefendant = array_count_values(str_split($this->defendant))['#'] <= 1;

        return $isValidPlaintiff && $isValidDefendant;
    }

    /**
     * @param string $rolesContract
     * @return int
     */
    private function getPoints(string $rolesContract):int{
        $roles = [King::class, Notary::class, Validator::class];
        $rolesContract = str_split($rolesContract);

        // Get the points and roles
        $sumPoints = $this->getSumOfPoints($rolesContract, $roles);
        $signsRoles = $this->getSignsRoles($rolesContract, $roles);

        // Go through the rules and setting new points if needed
        $this->setNewPoints($signsRoles, $sumPoints);

        return $sumPoints;
    }

    /**
     * @param String[] $rolesContract
     * @param BaseRole[] $roles
     *
     * @return int
     */
    private function getSumOfPoints(array $rolesContract, array $roles): int {
        $pointsByRole = 0;
        foreach ($rolesContract as $roleContract){
            foreach ($roles as $role){
                if($roleContract == $role::ACRONYM){
                    $pointsByRole+= (new $role)->getPoints();
                }
            }
        }

        return $pointsByRole;
    }

    /**
     * @param String[] $rolesContract
     * @param BaseRole[] $roles
     *
     * @return array
     */
    private function getSignsRoles(array $rolesContract, array $roles): array {
        $rolesValidated = [];
        foreach ($rolesContract as $roleContract){
            foreach ($roles as $role){
                if($roleContract == $role::ACRONYM){
                    $rolesValidated[] = $role;
                }
            }
        }

        return $rolesValidated;
    }

    /**
     * @param BaseRole[] $rolesValidated
     * @param int $sumPoints
     */
    private function setNewPoints(array $rolesValidated, int &$sumPoints): void {
        foreach ($rolesValidated as $roleValidated){
            $rules = (new $roleValidated)->getRules();
            if(isset($rules['RemovesPointTo']) && in_array($rules['RemovesPointTo'], $rolesValidated)){
                $sumPoints = $sumPoints - (new $rules['RemovesPointTo'])->getPoints();
            }
        }
    }
}