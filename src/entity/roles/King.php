<?php

namespace Entity\Roles;

/**
 * Class King
 * @package Entity\Roles
 */
class King extends BaseRole
{
    const ACRONYM = 'K';

    public function __construct()
    {
        $this->points = 5;
        $this->rules = [
            'RemovesPointTo' => Validator::class
        ];
    }
}