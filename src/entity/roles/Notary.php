<?php

namespace Entity\Roles;

/**
 * Class Notary
 * @package Entity\Roles
 */
class Notary extends BaseRole
{
    const ACRONYM = 'N';

    public function __construct()
    {
        $this->points = 2;
    }
}