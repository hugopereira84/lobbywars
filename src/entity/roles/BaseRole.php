<?php

namespace Entity\Roles;

/**
 * Class BaseRole
 * @package Entity\Roles
 */
class BaseRole
{
    /**
     * @var int
     */
    protected $points;

    /**
     * @var string
     */
    protected $acronym;

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     * @return BaseRole
     */
    public function setPoints($points)
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * @param mixed $acronym
     * @return BaseRole
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
        return $this;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     * @return BaseRole
     */
    public function setRules(array $rules): BaseRole
    {
        $this->rules = $rules;
        return $this;
    }
}