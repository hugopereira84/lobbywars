<?php

namespace Entity\Roles;

/**
 * Class Validator
 * @package Entity\Roles
 */
class Validator extends BaseRole
{
    const ACRONYM = 'V';

    public function __construct()
    {
        $this->points = 1;
    }
}