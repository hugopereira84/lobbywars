# Lobby wars
At the moment docker-compose is not working.
To run the project, run the following command in src folder: $ php -S localhost:8000

## Project Structure


## How to run Docker

First, you need to make sure you have installed ```docker``` and ```docker-compose```. 
    
Once you have all of that, simply open a terminal and navigate to the directory where ```docker-compose.yml``` 
is located, then run:
```
docker-compose build
```
then
```
docker-compose up -d